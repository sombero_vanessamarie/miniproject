package tn.icm.test;

import tn.icm.lib.Classroom;
import org.junit.Test;
import java.math.BigDecimal;

public class ClassroomFactoryForTest {

	private MockValues mockValues = new MockValues();

	public Classroom newClassroom() {

		BigDecimal classroomId = mockValues.nextBigDecimal();

		Classroom classroom = new Classroom();
		classroom.setClassroomId(classroomId);
		return classroom;
	}

	@Test
	public void ClassroomFactoryForTest() {

	}

}
