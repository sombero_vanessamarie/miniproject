package tn.icm.test;

import tn.icm.lib.jpa.StudentEntity;
import org.junit.Test;
import java.math.BigDecimal;

public class StudentEntityFactoryForTest {

	private MockValues mockValues = new MockValues();

	public StudentEntity newStudentEntity() {

		BigDecimal studentId = mockValues.nextBigDecimal();

		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setStudentId(studentId);
		return studentEntity;
	}

	@Test
	public void StudentEntityFactoryForTest() {

	}
}
