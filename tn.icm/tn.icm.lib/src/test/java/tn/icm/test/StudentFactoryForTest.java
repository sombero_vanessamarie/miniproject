package tn.icm.test;

import tn.icm.lib.Student;
import org.junit.Test;
import java.math.BigDecimal;

public class StudentFactoryForTest {

	private MockValues mockValues = new MockValues();

	public Student newStudent() {

		BigDecimal studentId = mockValues.nextBigDecimal();

		Student student = new Student();
		student.setStudentId(studentId);
		return student;
	}

	@Test
	public void StudentFactoryForTest() {

	}
}
