/*
 * Created on 6 Sep 2017 ( Time 18:02:44 )
 */
package tn.icm.business.service.mapping;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import tn.icm.lib.Classroom;
import tn.icm.lib.jpa.ClassroomEntity;
import tn.icm.lib.jpa.StudentEntity;
import tn.icm.test.MockValues;

/**
 * Test : Mapping between entity beans and display beans.
 */
public class ClassroomServiceMapperTest {

	private ClassroomServiceMapper classroomServiceMapper;

	private static ModelMapper modelMapper = new ModelMapper();

	private MockValues mockValues = new MockValues();

	@BeforeClass
	public static void setUp() {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	@Before
	public void before() {
		classroomServiceMapper = new ClassroomServiceMapper();
		classroomServiceMapper.setModelMapper(modelMapper);
	}

	/**
	 * Mapping from 'ClassroomEntity' to 'Classroom'
	 *
	 * @param classroomEntity
	 */
	@Test
	public void testMapClassroomEntityToClassroom() {
		// Given
		ClassroomEntity classroomEntity = new ClassroomEntity();
		classroomEntity.setClassroomNo(mockValues.nextBigDecimal());
		classroomEntity.setStudentfk(new StudentEntity());
		classroomEntity.getStudentfk().setStudentId(mockValues.nextBigDecimal());

		// When
		Classroom classroom = classroomServiceMapper.mapClassroomEntityToClassroom(classroomEntity);

		// Then
		assertEquals(classroomEntity.getClassroomNo(), classroom.getClassroomNo());
		assertEquals(classroomEntity.getStudentfk().getStudentId(), classroom.getStudentId());
	}

	/**
	 * Test : Mapping from 'Classroom' to 'ClassroomEntity'
	 */
	@Test
	public void testMapClassroomToClassroomEntity() {
		// Given
		Classroom classroom = new Classroom();
		classroom.setClassroomNo(mockValues.nextBigDecimal());
		classroom.setStudentId(mockValues.nextBigDecimal());

		ClassroomEntity classroomEntity = new ClassroomEntity();

		// When
		classroomServiceMapper.mapClassroomToClassroomEntity(classroom, classroomEntity);

		// Then
		assertEquals(classroom.getClassroomNo(), classroomEntity.getClassroomNo());
		assertEquals(classroom.getStudentId(), classroomEntity.getStudentfk().getStudentId());
	}

	@Test
	public void testMapClassroomEntityNullToClassroom() {
		// Given
		ClassroomEntity classroomEntity = null;

		// When
		Classroom classroom = classroomServiceMapper.mapClassroomEntityToClassroom(classroomEntity);

		// Then
		assertNull(classroom);
	}

	@Test
	public void testMapClassroomEntityToClassroomNull() {
		// Given
		ClassroomEntity classroomEntity = new ClassroomEntity();
		classroomEntity.setClassroomNo(mockValues.nextBigDecimal());
		classroomEntity.setStudentfk(new StudentEntity());
		classroomEntity.getStudentfk().setStudentId(mockValues.nextBigDecimal());

		// When
		Classroom classroom = null;

		// Then
		classroomServiceMapper.mapClassroomToClassroomEntity(classroom, classroomEntity);
	}

}