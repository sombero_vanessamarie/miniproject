package tn.icm.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import java.math.BigDecimal;
import tn.icm.lib.jpa.StudentEntity;

/**
 * Repository : Student.
 */
public interface StudentJpaRepository extends PagingAndSortingRepository<StudentEntity, BigDecimal> {

}
