package sg.gov.ntp.tn.icm.facade;

import java.math.BigDecimal;

import tn.icm.lib.Student;

public interface StudentFacade {

	Student saveStudent(Student student);

	Student getStudent(BigDecimal id);

}
