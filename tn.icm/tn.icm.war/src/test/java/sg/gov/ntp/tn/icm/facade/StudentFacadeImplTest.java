package sg.gov.ntp.tn.icm.facade;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import tn.icm.business.service.StudentService;
import tn.icm.lib.Student;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class StudentFacadeImplTest {

  @Mock
  StudentService studentService;

  @InjectMocks
  StudentFacadeImpl studentFacadeImpl;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void saveStudent() throws Exception {

    Student student = new Student();
    student.setStudentId(new BigDecimal(15));
    student.setFname("Vanessa");
    student.setLname("Sombero");
    student.setAge(new BigDecimal(23));
    student.setGender("f");
    student.setCreatedBy("v");
    student.setCreatedDt(null);
    student.setLastUpdatedBy("1");
    student.setLastUpdatedDt(null);
    student.setServerName("1");
    student.setSystemName("1");
    student.setVerNo(null);
    studentFacadeImpl.saveStudent(student);

    Mockito.when(studentService.create(Mockito.any(Student.class)))
        .thenReturn(student);

    studentFacadeImpl.saveStudent(Mockito.any(Student.class));
    Mockito.verify(studentService).create(Mockito.any(Student.class));
  }

  @Test
  public void getStudent() throws Exception {

    Student student = new Student();
    student.setStudentId(new BigDecimal(15));
    student.setFname("Vanessa");
    student.setLname("Sombero");
    student.setAge(new BigDecimal(23));
    student.setGender("f");
    student.setCreatedBy("v");
    student.setCreatedDt(null);
    student.setLastUpdatedBy("1");
    student.setLastUpdatedDt(null);
    student.setServerName("1");
    student.setSystemName("1");
    student.setVerNo(null);
    studentFacadeImpl.getStudent(new BigDecimal(1));

    Mockito.when(studentService.findById(Mockito.any(BigDecimal.class)))
        .thenReturn(student);

    studentFacadeImpl.getStudent(Mockito.any(BigDecimal.class));
    Mockito.verify(studentService).findById(Mockito.any(BigDecimal.class));
  }

}
